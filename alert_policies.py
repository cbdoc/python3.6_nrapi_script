#!bin\python
#.py file now an executable script

# Author: Naval Monga
# Purpose: To create standardized & automated alert definitions for past/present/upcoming deployments.
# Python Configuration: Python 3.6.2
# Reference: newAlert.py Scripted by Keagan Peet

# Package imports
import json
import requests
import yaml

# Alert Policy CREATE data.
API_KEY = ''
inc_creation = "PER POLICY"  # Incident is created per policy violation.
request_headers = {} # Request headers
policy_names = []
policy_id_dict = {}
aConditionID = 0
polID = 0
eAdd = []
sAdd = []

# Create a New Relic Alert Policy.
def CreateNewPolicy(ip, pn):
    global polID  # assign as global so we can modify the var
    payload1 = {
        "policy": {
            "incident_preference": ip,
            "name": pn
        }
    }
    print ("Creating Policy: " + str(pn))
    pol_url = 'https://api.newrelic.com/v2/alerts_policies.json'
    policyCreate = requests.post(pol_url, data=json.dumps(payload1), headers=request_headers)
    if policyCreate.status_code == 201:
        pol_response = policyCreate.content
        json_formatted = json.loads(pol_response)
        polID = json_formatted['policy']['id']
        print ("Policy Number: " + str(polID) + " successfully created. Storing ID")
        return polID
    else:
        print("Status Code: " + str(policyCreate.status_code))
        print("Policy Creation Failed!")
        print(policyCreate.content)

# Index current policies on New Relic account.
def CurrentPolicyList():
    global policy_id_dict
    response = []
    print("Retrieving list of the alert policies associated with your New Relic account.")

    policy_URL = 'https://api.newrelic.com/v2/alerts_policies.json'
    policyList = requests.get(policy_URL, headers=request_headers)

    if policyList.status_code == 200:
        pol_response = json.loads(policyList.content)
        for val in pol_response['policies']:
           response.append(val['name'])
           policy_id_dict.setdefault(val['name'], val['id'])
    else:
        print("Status Code: " + str(policyList.status_code))
        print("Policy Retrieval Failed!")
        print(policyList.content)
    return response

# Policy ID by name
def GetPolicyID(name):
    CurrentPolicyList()
    return policy_id_dict.get(name)

# Check if condition with 'cName' exists under policy of polID.
def ExistingCondition(polID, cName):
    api = 'https://api.newrelic.com/v2/alerts_conditions.json'
    payload = {'policy_id': polID}

    policyList = requests.get(api, headers=request_headers,params=payload)

    if policyList.status_code == 200:
        pol_response = json.loads(policyList.content)
        for val in pol_response['conditions']:
            if val['name'] == cName:
                return True
        return False
    else:
        print("Status Code: " + str(policyList.status_code))
        print("Condition Retrieval Failed! here")

# Get a condition ID corresponding to policy and condition name.
def GetConditionID(policyID, condition):
    api = 'https://api.newrelic.com/v2/alerts_conditions.json'
    payload = {'policy_id': policyID}

    policyList = requests.get(api, headers=request_headers, params=payload)

    if policyList.status_code == 200:
        pol_response = json.loads(policyList.content)
        for ids in pol_response['conditions']:
            if ids['name'] == condition:
                return ids['id']
    else:
        print("Status Code: " + str(policyList.status_code))
        print("Condition Retrieval Failed! here")

# Get a condition ID corresponding to polict and Infrastructure condition name.
def GetInfraID(policyID, condition):
    api = 'https://infra-api.newrelic.com/v2/alerts/conditions?policy_id=' + str(policyID)

    policyList = requests.get(api, headers=request_headers)

    if policyList.status_code == 200:
        pol_response = json.loads(policyList.content)
        for ids in pol_response['data']:
            if ids['name'] == condition:
                return ids['id']
    else:
        print("Status Code: " + str(policyList.status_code))
        print("Infra Condition Retrieval Failed! here2")

# Check if Infra condition with 'iName' exists under policy of polID.
def ExistingInfraCondition(polID, iName):
    api = 'https://infra-api.newrelic.com/v2/alerts/conditions?policy_id=' + str(polID)

    policyList = requests.get(api, headers=request_headers)

    if policyList.status_code == 200:
        pol_response = json.loads(policyList.content)
        for ids in pol_response['data']:
            if ids['name'] == iName:
                return True
        return False
    else:
        print("Status Code: " + str(policyList.status_code))
        print("Infra Condition Retrieval Failed! here")
# Create a New Relic APM Alert Condition for polID.
    
def CreateAPMCondition(polID, metricType, condTitle, condMetric, condDuration, condCriticalT, condWarnT, condOperator):
    if not ExistingCondition(polID, condTitle):
        api = 'https://api.newrelic.com/v2/alerts_conditions/policies/' + str(polID) + '.json' #policy ID used here to assign conditions
        payload = {
            "condition": {
                "type": metricType,
                "name": condTitle,
                "enabled": "true",
                "condition_scope": "application",
                "entities": [

                ],
            "metric": condMetric,
            "violation_close_timer": "8",
            "terms": [
            {
                "duration": str(condDuration),
                "operator": condOperator,
                "priority": "critical",
                "threshold": str(condCriticalT),
                "time_function": "all"
            },
            {
                "duration": str(condDuration),
                "operator": condOperator,
                "priority": "warning",
                "threshold": str(condWarnT),
                "time_function": "all"
            }]
            }
        }

      # post condition
        print ("Creating APM Condition: " + str(condTitle))
        apm_req = requests.post(api, json=payload, headers=request_headers)
        if apm_req.status_code == 201:
            print(condTitle + ' was successfully created!')
            resp = apm_req.json()
            if resp['condition']['type'] in ('apm_app_metric', 'apm_kt_metric'):
                anID = resp['condition']['id']
                return anID
            else:
                print ("Unknown condition type!")
        else:
            print('Fail! Response code: ' + str(apm_req.status_code))
            print(apm_req.content)
    else:
        print(f"Alert condition: {condTitle} has already been created, updating condition...")
        UpdateAPMCondition(polID, metricType, condTitle, condMetric, condDuration, condCriticalT, condWarnT, condOperator);

# Update New Relic APM Alert Condition for polID.
def UpdateAPMCondition(polID, metricType, condTitle, condMetric, condDuration, condCriticalT, condWarnT, condOperator):
    api = 'https://api.newrelic.com/v2/alerts_conditions/' + str(GetConditionID(polID, condTitle)) + '.json'  # policy ID used here to assign conditions
    payload = {
        "condition": {
            "type": metricType,
            "name": condTitle,
            "enabled": "true",
            "condition_scope": "application",
            "entities": [

            ],
            "metric": condMetric,
            "violation_close_timer": "8",
            "terms": [
                {
                    "duration": str(condDuration),
                    "operator": condOperator,
                    "priority": "critical",
                    "threshold": str(condCriticalT),
                    "time_function": "all"
                },
                {
                    "duration": str(condDuration),
                    "operator": condOperator,
                    "priority": "warning",
                    "threshold": str(condWarnT),
                    "time_function": "all"
                }]
        }
    }

    # put condition
    print("Updating APM Condition: " + str(condTitle))
    apm_req = requests.put(api, json=payload, headers=request_headers)
    if apm_req.status_code == 200:
        print(condTitle + ' was successfully updated!')
        resp = apm_req.json()
        if resp['condition']['type'] in ('apm_app_metric', 'apm_kt_metric'):
            anID = resp['condition']['id']
            return anID
        else:
            print("Unknown condition type!")
    else:
        print('Fail! Response code: ' + str(apm_req.status_code))
        print(apm_req.content)

# Create a New Relic User Defined Condition  for polID.
def CreateUserDefinedCondition(polID, metricType, condTitle, condMetric, condDuration, condCriticalT, condWarnT,
                               condOperator, userMetric, userValue):
    if not ExistingCondition(polID, condTitle):
        api = 'https://api.newrelic.com/v2/alerts_conditions/policies/' + str(
            polID) + '.json'  # policy ID used here to assign conditions
        payload = {
            "condition": {
                "type": metricType,
                "name": condTitle,
                "enabled": "true",
                "condition_scope": "application",
                "entities": [

                ],
                "metric": condMetric,
                "violation_close_timer": "8",
                "terms": [
                    {
                        "duration": str(condDuration),
                        "operator": condOperator,
                        "priority": "critical",
                        "threshold": str(condCriticalT),
                        "time_function": "all"
                    },
                    {
                        "duration": str(condDuration),
                        "operator": condOperator,
                        "priority": "warning",
                        "threshold": str(condWarnT),
                        "time_function": "all"
                    }],
                "user_defined": {
                    "metric": str(userMetric),
                    "value_function": str(userValue)
                }
            }
        }

        # post condition
        print ("Creating User Defined Condition: " + str(condTitle))
        apm_req = requests.post(api, json=payload, headers=request_headers)
        if apm_req.status_code == 201:
            print(condTitle + ' was successfully created!')
            resp = apm_req.json()
            if resp['condition']['type'] in ('apm_app_metric', 'apm_kt_metric'):
                anID = resp['condition']['id']
                return anID
            else:
                print ("Unknown condition type!")
        else:
            print('Fail! Response code: ' + str(apm_req.status_code))
            print(apm_req.content)
    else:
        print(f"Alert condition: {condTitle} has already been created, updating condition...")
        UpdateUserCondition(polID, metricType, condTitle, condMetric, condDuration, condCriticalT, condWarnT, condOperator, userMetric, userValue);

# Update New Relic User Defined Condition for polID.
def UpdateUserCondition(polID, metricType, condTitle, condMetric, condDuration, condCriticalT, condWarnT,
                               condOperator, userMetric, userValue):
    api = 'https://api.newrelic.com/v2/alerts_conditions/' + str(GetConditionID(polID, condTitle)) + '.json'  # policy ID used here to assign conditions
    payload = {
        "condition": {
            "type": metricType,
            "name": condTitle,
            "enabled": "true",
            "condition_scope": "application",
            "entities": [

            ],
            "metric": condMetric,
            "violation_close_timer": "8",
            "terms": [
                {
                    "duration": str(condDuration),
                    "operator": condOperator,
                    "priority": "critical",
                    "threshold": str(condCriticalT),
                    "time_function": "all"
                },
                {
                    "duration": str(condDuration),
                    "operator": condOperator,
                    "priority": "warning",
                    "threshold": str(condWarnT),
                    "time_function": "all"
                }],
            "user_defined": {
                "metric": str(userMetric),
                "value_function": str(userValue)
            }
        }
    }

    # post condition
    print("Updating User Defined Condition: " + str(condTitle))
    apm_req = requests.put(api, json=payload, headers=request_headers)
    if apm_req.status_code == 200:
        print(condTitle + ' was successfully updated!')
        resp = apm_req.json()
        if resp['condition']['type'] in ('apm_app_metric', 'apm_kt_metric'):
            anID = resp['condition']['id']
            return anID
        else:
            print("Unknown condition type!")
    else:
        print('Fail! Response code: ' + str(apm_req.status_code))
        print(apm_req.content)

# Get APM app entity ID.
def GetAPMEntityID(anAppName):
    entityapm = 'https://api.newrelic.com/v2/applications.json'
    print("Getting APM Entity ID for: " + str(anAppName))
    payload = {'filter[name]': str(anAppName)}
    r = requests.get(entityapm, headers=request_headers, params=payload)
    if r.status_code == 200:
        response = r.json()
        for data in response['applications']:
            if data['name'] == anAppName:
                id = data['id']
        return id
    else:
        print("Error! Status code: " + str(r.status_code))
        print(r.content)

# Assign app entity to created condition.
def AssignAPMEntityToCondition(aConditionID, aentityID):
    print(f"Assigning defined APM entity to condition with id: {aConditionID}")
    api = 'https://api.newrelic.com/v2/alerts_entity_conditions/' + str(aentityID) + '.json'
    payload = {'entity_type': 'Application', 'condition_id': aConditionID}
    r = requests.put(api, headers=request_headers, params=payload)
    if str(r.status_code)[:1] == '2':
        print("Success! Application entity: " + str(aentityID) + " assigned!")
    else:
        print("Failed with status code: " + str(r.status_code))
        print(r.content)

# Create New Relic Infrastructure Condition for polID.
def CreateInfraCondition(polID, iType, iName, iFilter, ieType, iValue, iCompare, iCrit, iWarn, iCritD, iWarnD):
    if not ExistingInfraCondition(polID, iName):
        api = 'https://infra-api.newrelic.com/v2/alerts/conditions'
        #can change to variables via cond_template file
        payload = {
            "data":{
                "type":iType,
                "name":iName,
                "enabled": bool("true"),
                "where_clause": iFilter,
                "policy_id": polID,
                "event_type":ieType,
                "select_value":iValue,
                "comparison":iCompare,
                "critical_threshold":{
                    "value": iCrit,
                    "duration_minutes": iCritD,
                    "time_function":"all" #can be all or any-- all= "for at least", any = "at least once in"
                },
                "warning_threshold":{
                    "value": iWarn,
                    "duration_minutes": iWarnD,
                    "time_function":"all"
                }
            }
        }

        print("Creating Infrastructure Condition: " + str(iName))
        infra_req = requests.post(api, json=payload, headers=request_headers)
        if infra_req.status_code == 201:
            print(str(iName) + ' was successfully created!')
        else:
            print('Fail! Response code: ' + str(infra_req.status_code))
            print(infra_req.content)
    else:
        print(f"Alert condition: {iName} has already been created, updating condition...")
        UpdateInfraCondition(polID, iType, iName, iFilter, ieType, iValue, iCompare, iCrit, iWarn, iCritD, iWarnD);

# Updated New Relic Infrastructure Condition for polID.
def UpdateInfraCondition(polID, iType, iName, iFilter, ieType, iValue, iCompare, iCrit, iWarn, iCritD, iWarnD):
    api = 'https://infra-api.newrelic.com/v2/alerts/conditions/' + str(GetInfraID(polID, iName))
    # can change to variables via cond_template file
    payload = {
        "data": {
            "type": iType,
            "name": iName,
            "enabled": bool("true"),
            "where_clause": iFilter,
            "policy_id": polID,
            "event_type": ieType,
            "select_value": iValue,
            "comparison": iCompare,
            "critical_threshold": {
                "value": iCrit,
                "duration_minutes": iCritD,
                "time_function": "all"  # can be all or any-- all= "for at least", any = "at least once in"
            },
            "warning_threshold": {
                "value": iWarn,
                "duration_minutes": iWarnD,
                "time_function": "all"
            }
        }
    }

    print("Updating Infrastructure Condition: " + str(iName))
    infra_req = requests.put(api, json=payload, headers=request_headers)
    if infra_req.status_code == 200:
        print(str(iName) + ' was successfully updated!')
    else:
        print('Fail! Response code: ' + str(infra_req.status_code))
        print(infra_req.content)

def CreateNRQLConditon(polID, nName, nDuration, nOperator, nPriority, nThreshold, nTimeFunc, nValueFunc, nQuery, nSinceValue):
    if not NRQLCondition(polID, nName):
        endpoint = 'https://api.newrelic.com/v2/alerts_nrql_conditions/policies/' + str(polID) + '.json'
        payload = {
            "nrql_condition": {
            "name": nName,
            "enabled": bool("true"),
            "terms": [
            {
                "duration": nDuration,
                "operator": nOperator,
                "priority": nPriority,
                "threshold": nThreshold,
                "time_function": nTimeFunc
            }
            ],
            "value_function": nValueFunc,
            "nrql": {
                "query": nQuery,
                "since_value": nSinceValue
            }
            }
        }

        print("Creating NRQL condition: " + str(nName) + "...")
        try:
            r = requests.post(endpoint, headers=request_headers, json=payload)
            if str(r.status_code)[:1] == '2':
                resp = r.json()
                print("Success! Created NRQL Condition: " + str(nName))
            else:
                print("Error Occurred! Status Code: " + str(r.status_code))
                print(r.content)
        except IndexError:
            return 'ER'
    else:
        print(f"Alert condition: {nName} has already been created, moving on...")

def NRQLCondition(polID, nName):
    api = 'https://api.newrelic.com/v2/alerts_nrql_conditions.json'
    payload = {'policy_id': polID}

    policyList = requests.get(api, headers=request_headers,params=payload)

    if policyList.status_code == 200:
        pol_response = json.loads(policyList.content)
        for obj in pol_response['nrql_conditions']:
            if obj['name'] == nName:
                return True
        return False
    else:
        print("Status Code: " + str(policyList.status_code))
        print("Policy Retrieval Failed! here")

# def getChannelIDs():
#     api = 'https://api.newrelic.com/v2/alerts_channels.json'
#     head = {'X-Api-Key': API_KEY}
#     cycle = 0
#     try_again = 1
#     first_time = 1
#     channel_dict = {}  # key value lookup for email-id
#     try:
#         print('Obtaining Channel IDs...')
#         while try_again == 1:
#             payload = {'page': cycle}
#             r = requests.get(api, headers=head, params=payload)
#             print('Requesting Channels Status Code= ' + str(r.status_code) + '\nCycle: ' + str(cycle))
#             channels = r.json()
#             if str(r.status_code)[:1] == '2':
#                 for aChannel in channels['channels']:
#                     email = aChannel['name']
#                     nID = aChannel['id']
#                     channel_dict.update({email: nID})
#                 if 'last' not in r.links:
#                     try_again = 0
#             elif str(r.status_code)[:1] == '4':
#                 print("Error! Invalid request-Check API key or inputs")
#             else:
#                 print("Could not complete request.")
#         return channel_dict
#     except IndexError:
#         return 'ER'
# #Takes list of emails to assign to policy originally created
# def AssignChannels(emailsToAdd, polID):
#     api = 'https://api.newrelic.com/v2/alerts_policy_channels.json'
#     channelIDs = getChannelIDs()
#     emailList = []
#     try:
#         print("Adding channels to policy for desired emails")
#         request_headers = {'X-Api-Key': API_KEY}
#         for email in emailsToAdd:
#             if email in channelIDs.keys():
#                 emailList.append(channelIDs[email]) #add id to var based on email(key)
#             else:
#                 print('Email ' + str(email) + ' not found, attempting to create channel...')
#                 newEmailID = CreateEmailChannel(email)
#                 emailList.append(newEmailID) # add newly created ID to list of channels to add to policy
#         emailString = ",".join([str(x) for x in emailList]) # create comma sep list
#         payload = {"policy_id": str(polID), "channel_ids": str(emailString)}
#         r = requests.put(api,headers=request_headers,params=payload)
#         if str(r.status_code)[:1] == '2':
#             resp = r.json()
#             print("Added emails successfully!")
#         else:
#             print("Error Occurred: " + str(r.status_code))
#             print(r.content)
#     except IndexError:
#         return 'ER'
#
# def AssignPDChanneltoPolicy(pdTitle, pdKey):
#     try:
#         print("Adding PagerDuty Channel to policy...")
#         api = 'https://api.newrelic.com/v2/alerts_policy_channels.json'
#         request_headers = {'X-Api-Key': admin_key}
#         pdID = CreatePagerDutyChannel(pdTitle, pdKey)
#         payload = {"policy_id": str(polID), "channel_ids": str(pdID)}
#         r = requests.put(api, headers=request_headers, params=payload)
#         if str(r.status_code)[:1] == '2':
#             resp = r.json()
#             print("Added PagerDuty Channel successfully!")
#         else:
#             print("Error Occurred: " + str(r.status_code))
#             print(r.content)
#     except IndexError:
#         return 'ER'
#
# def CreateEmailChannel(email):
#     endpoint = 'https://api.newrelic.com/v2/alerts_channels.json'
#     emailPayload = {
#             "channel": {
#             "name": str(email),
#             "type": "email",
#             "configuration": {
# 		          "recipients": str(email),
# 		          "include_json_attachment": bool("true")
#                   }
#              }
#     }
#     try:
#         r = requests.post(endpoint, headers=request_headers, json=emailPayload)
#         if str(r.status_code)[:1] == '2':
#             resp = r.json()
#             newID = resp['channels'][0]['id'] #store ID of new channel created
#             return newID #return ID to assign to current policy
#     except IndexError:
#         return 'ER'

def CreatePagerDutyChannel(pdTitle, pdKey, polID):
    endpoint = 'https://api.newrelic.com/v2/alerts_channels.json?policy_ids[]=' + str(polID)
    pdPayload = {
            "channel": {
            "name": str(pdTitle),
            "type": "pagerduty",
            "configuration": {
                "service_key": str(pdKey)
                }
            }
    }
    try:
        r = requests.post(endpoint, headers=request_headers, json=pdPayload)
        if str(r.status_code)[:1] == '2':
            print(f"Assigned {pdTitle} distribution to policy with ID: {polID}")
            resp = r.json()
            newPDID = resp['channels'][0]['id'] #store ID of new channel created
            return newPDID #return ID to assign to current policy
    except IndexError:
        return 'ER'

# Takes an email & create/assign notification channel to policy conditions.
def CreateAlertChannel(channel, polID):
    endpoint = 'https://api.newrelic.com/v2/alerts_channels.json?policy_ids[]=' + str(polID)
    emailPayload = {
        "channel": {
            "name": channel,
            "type": "email",
            "configuration": {
                "recipients": channel,
                "include_json_attachment": bool("true")
            }
        }
    }
    try:
        r = requests.post(endpoint, headers=request_headers, json=emailPayload)
        if str(r.status_code)[:1] == '2':
            print(f"Assigned {channel} distribution to policy with ID: {polID}")
            resp = r.json()
            newID = resp['channels'][0]['id'] #store ID of new channel created
            return newID #return ID to assign to current policy
    except IndexError:
        return 'ER'

# Takes a Slack workspace URL & create/assign notification channel to policy conditions.
def CreateSlackChannel(name, url, polID):
    endpoint = 'https://api.newrelic.com/v2/alerts_channels.json?policy_ids[]=' + str(polID)
    emailPayload = {
        "channel": {
            "name": name,
            "type": "slack",
            "configuration": {
                "url": url,
                "channel": "#newrelic_alerts"
            }
        }
    }
    try:
        r = requests.post(endpoint, headers=request_headers, json=emailPayload)
        if str(r.status_code)[:1] == '2':
            print(f"Assigned {name} distribution to policy with ID: {polID}")
            resp = r.json()
            newID = resp['channels'][0]['id']  # store ID of new channel created
            return newID  # return ID to assign to current policy
    except IndexError:
        return 'ER'

def main():
    # open and parse config_template.yml file
    with open("config_template.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile) #load config.yml

        global API_KEY  # New Relic Non-Prod key
        global inc_creation # Incident preference
        global request_headers # Payload
        global policy_name # Unique policy identifier
        global aConditionID # Condition ID
        global eAdd
        global sAdd
        global pDuty

        # Global configuration for New Relic Alerts API
        API_KEY = cfg['GLOBAL']['admin_key']
        inc_creation = cfg['GLOBAL']['incident_preference']
        request_headers = {'X-Api-Key': str(API_KEY), 'Content-Type': 'application/json'}
        policy_name = cfg['GLOBAL']['policy_name'] # Load policies to be created.
        eAdd = cfg['GLOBAL']['email_dist']
        sAdd = cfg['GLOBAL']['slack_channel']
        pDuty = cfg['GLOBAL']['pagerduty_channel']

    # Check for existing policy name
    if policy_name in CurrentPolicyList():
        print(f"An alert policy '{policy_name}' has already been defined, checking for updated conditions...")
    else:
        # Create new policy in Non-Prod NR.
        CreateNewPolicy(inc_creation, policy_name)

    # open and parse cond_template.yml file
    with open("cond_template.yml", "r") as cond_temp:
        cond_file = yaml.load_all(cond_temp)
        for data in cond_file:
            if 'APM' in data.keys():
                apm_cond = data['APM']['apm_condition_names']
                if not apm_cond:
                    print("No APM condition specified for policy, moving on...")
                else:
                    k = 0
                    app_name = data['APM']['app_name']  # ***THIS SHOULD MATCH APP_NAMES CONFIGURED IN APM AGENT YML
                    mType = data['APM']['metric_types']
                    cNames = data['APM']['apm_condition_names']
                    cMetrics = data['APM']['apm_condition_metrics']
                    cDurations = data['APM']['apm_condition_duration']
                    cCrit = data['APM']['apm_condition_critT']
                    cWarn = data['APM']['apm_condition_warnT']
                    cOps = data['APM']['apm_cond_operators']
                    userMetric = data['APM']['apm_custom_metrics']
                    userValue = data['APM']['apm_value_functions']
                    while k < len(apm_cond):
                        z = 0  # counter for custom metrics- dependent on if 'user_defined' is specified in apm_condition_metrics
                        if cMetrics[k] == 'user_defined':
                            aConditionID = CreateUserDefinedCondition(GetPolicyID(policy_name), mType[k], cNames[k], cMetrics[k],
                                                                      cDurations[k], cCrit[k], cWarn[k], cOps[k],
                                                                      userMetric[z], userValue[z])
                            z += 1
                        else:
                            aConditionID = CreateAPMCondition(GetPolicyID(policy_name), mType[k], cNames[k], cMetrics[k],
                                                              cDurations[k], cCrit[k], cWarn[k], cOps[k])

                        if not aConditionID:
                            aConditionID = GetConditionID(GetPolicyID(policy_name), cNames[k])
                            aentityID = GetAPMEntityID(''.join(app_name))
                            AssignAPMEntityToCondition(aConditionID, aentityID)
                            k += 1
                            continue
                        else:
                            aentityID = GetAPMEntityID(''.join(app_name))
                            AssignAPMEntityToCondition(aConditionID, aentityID)
                        k += 1
                    continue
            if 'INFRA' in data.keys():
                infra_cond = data['INFRA']['infra_condition_names']
                if not infra_cond:
                    print("No Infrastructure condition specified for policy, moving on...")
                else:
                    p = 0
                    pType = data['INFRA']['infra_types']
                    pNames = data['INFRA']['infra_condition_names']
                    peType = data['INFRA']['eventType']
                    pFilter = data['INFRA']['filterClause']
                    pValue = data['INFRA']['selectValue']
                    pCompare = data['INFRA']['infra_comparison']
                    pCrit = data['INFRA']['criticalT']
                    pWarn = data['INFRA']['warningT']
                    pCritD = data['INFRA']['crit_durations']
                    pWarnD = data['INFRA']['warn_durations']
                    while p < len(infra_cond):
                        CreateInfraCondition(GetPolicyID(policy_name), pType[p], pNames[p], pFilter[p], peType[p], pValue[p], pCompare[p],
                                             pCrit[p], pWarn[p], pCritD[p], pWarnD[p])
                        p += 1

        # Assign channels to policies if not blank in config
        if eAdd == []:
            print ("No email notification channels to add! Skipping...")
        else:
            for email in eAdd:
                CreateAlertChannel(email, GetPolicyID(policy_name))
        if not sAdd['channel_name'] or not sAdd['channel_url']:
            print ("No Slack notification channels to add! Skipping...")
        else:
            CreateSlackChannel(sAdd['channel_name'],sAdd['channel_url'], GetPolicyID(policy_name))
        if not pDuty['pagerduty_title'] or not pDuty['pagerduty_key']:
            print ("No PagerDuty notification channels to add! Skipping...")
        else:
            CreatePagerDutyChannel(pDuty['pagerduty_title'], pDuty['pagerduty_key'], GetPolicyID(policy_name))
main()


